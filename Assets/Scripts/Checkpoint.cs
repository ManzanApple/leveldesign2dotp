﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Checkpoint : MonoBehaviour
{
    public static Checkpoint activeCheckpoint;
    public static bool deletePreviousCheckpoints = false;

    public static void SetActiveCheckpoint(Checkpoint checkpoint)
    {
        if (activeCheckpoint == checkpoint) return;

        if (activeCheckpoint != null)
        {
            if (deletePreviousCheckpoints)
            {
                Destroy(activeCheckpoint.gameObject);
            }
            else
            {
                foreach (var system in activeCheckpoint.particleSystems)
                {
                    system.enableEmission = false;
                }
            }
        }

        activeCheckpoint = checkpoint;

        foreach (var system in activeCheckpoint.particleSystems)
        {
            system.enableEmission = true;
        }
    }

    private List<ParticleSystem> particleSystems = new List<ParticleSystem>();

    private void Start()
    {
        particleSystems.AddRange(GetComponentsInChildren<ParticleSystem>());
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.layer == LayerMask.NameToLayer("Player"))
        {
            SetActiveCheckpoint(this);
        }
    }

    private BoxCollider bc;
    private Vector3 _colliderGizmoSize;

    private void OnDrawGizmos()
    {
        bc = GetComponent<BoxCollider>();
        _colliderGizmoSize = bc.size;
        Gizmos.DrawIcon(transform.position, "checkpoint icon.png", true);
        Gizmos.color = activeCheckpoint == this ? Color.yellow : Color.gray;
        Gizmos.DrawWireCube(transform.position, _colliderGizmoSize);
    }
}
