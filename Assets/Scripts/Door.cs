﻿using UnityEngine;
using System.Collections;

public class Door : MonoBehaviour
{
    //Has the player obtained they key?
    private bool _playerHasKey = false;

	private void OnCollisionEnter(Collision hit)
	{
        if(_playerHasKey && hit.collider.gameObject.layer == LayerMask.NameToLayer("Player"))
        {
            Open();
        }
	}

    private void Open()
    {
        Destroy(gameObject);
    }

    public void OnObtainKey()
    {
        _playerHasKey = true;
    }
}
