﻿using UnityEngine;
using System.Collections;


public class CrusherHead : MonoBehaviour
{
    private Crusher _crusher;

    private void Start()
    {
        _crusher = GetComponentInParent<Crusher>();
    }

	private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer("Player"))
        {
            _crusher.OnCrusherHeadTouch(other.GetComponent<PlayerTest>());
        }
    }
}
