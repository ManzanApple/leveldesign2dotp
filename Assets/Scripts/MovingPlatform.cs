﻿using UnityEngine;
using System.Collections;


public class MovingPlatform : Platform
{
    [Tooltip("Is the platform always moving or does it start moving once the player comes in contact with the platform?")]
    public bool alwaysActive = true;

    [Tooltip("Time it takes the platform to travel from Waypoint Start to Waypoint End.")]
    public float time = 5f;

    private Transform waypointStart, waypointEnd;
    private Vector3 waypointStartPos, waypointEndPos;
    private float _time = 0;
    private bool goBack = false;

    protected override void Start()
	{
        base.Start();

        waypointStart = transform.FindChild("Waypoint Start");
        waypointEnd = transform.FindChild("Waypoint End");
        waypointStartPos = waypointStart.position;
        waypointEndPos = waypointEnd.position;
	}
	
	protected override void Update()
	{
        base.Update();

        FreezeWaypointsPositions();

        if(goBack)
        {
            _time -= Time.deltaTime;

            if (_time < 0)
            {
                _time = 0;
                goBack = false;
            }
        }
        else
        {
            _time += Time.deltaTime;

            if (_time > time)
            {
                _time = time;
                goBack = true;
            }
        }

        _rb.position = Vector3.Lerp(waypointStart.position, waypointEnd.position, _time / time);
	}

    private void FreezeWaypointsPositions()
    {
        waypointStart.position = waypointStartPos;
        waypointEnd.position = waypointEndPos;
    }

    private void OnCollisionStay(Collision hit)
    {
        if(hit.collider.gameObject.layer == LayerMask.NameToLayer("Player"))
        {
            Vector3 v = new Vector3();
            hit.collider.GetComponent<CustomPlayer>().TransferVelocity(v);
        }
    }
}
