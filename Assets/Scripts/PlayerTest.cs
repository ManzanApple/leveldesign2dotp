﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody))]
public class PlayerTest : MonoBehaviour
{
    private Vector3 _originalPosition;
    private Quaternion _originalRotation;
    private Vector3 _originalScale;

    public float respawnTimer;
    private float _respawnTimer;

    private Rigidbody _rb;
	
	private void Start()
	{
        _rb = GetComponent<Rigidbody>();
        _respawnTimer = respawnTimer;
        _originalPosition = transform.position;
        _originalRotation = transform.rotation;
        _originalScale = transform.localScale;
	}
	
	private void Update()
	{
        _respawnTimer -= Time.deltaTime;

        if(_respawnTimer <= 0)
        {
            _respawnTimer = respawnTimer;

            Respawn();
        }
	}

    public void Respawn()
    {
        _rb.velocity = Vector3.zero;
        _rb.angularVelocity = Vector3.zero;
        transform.position = _originalPosition;
        transform.rotation = _originalRotation;
        transform.localScale = _originalScale;
    }
}
