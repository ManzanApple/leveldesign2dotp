﻿using UnityEngine;
using System.Collections;


public class FallingPlatform : Platform
{
    /// <summary>
    /// How does this platform fall.
    /// Step: Time to fall starts to count when the player first comes in contact with this platform and keeps counting. It does not reset the timer if the player stops contact.
    /// Contact: Time to fall only counts when the player comes in contact with this platform. Resets the timer if the player stops contact.
    /// </summary>
    public enum FallMode
    {
        Step, Contact
    }
    [Tooltip("How does this platform fall. \n Step: Time to fall starts to count when the player first comes in contact with this platform and keeps counting. It does not reset the timer if the player stops contact. \n Contact: Time to fall only counts when the player comes in contact with this platform. Resets the timer if the player stops contact.")]
    public FallMode fallMode;

    /// <summary>
    /// Interval between the triggering of the falling and the actual fall of this platform.
    /// </summary>
    [Tooltip("Interval between the triggering of the falling and the actual fall of this platform.")]
    public float fallTimer = 1f;
    private float _fallTimer;

    /// <summary>
    /// Indicates if this platform resets when a certain time has elapsed or if it stays fallen.
    /// </summary>
    [Tooltip("Indicates if this platform resets when a certain time has elapsed or if it stays fallen.")]
    public bool respawns = true;
    private Vector3 _originalPosition;

    /// <summary>
    /// Interval between the fall and the reset of this platform.
    /// </summary>
    [Tooltip("Interval between the fall and the reset of this platform.")]
    public float respawnTimer = 1f;
    private float _respawnTimer;

    //Sets the Step behaviour of this platform active or disables it.
    private bool _stepActive = true;

    //Indicates if this platform is falling or not.
    private bool _falling = false;

    //Sets the gravity for this object;
    private float _gravity = 9.8f;

    protected override void Start()
	{
        base.Start();

        Reset();
        _originalPosition = transform.position;
	}

    protected override void Update()
	{
        base.Update();

        if(_falling)
        {
            transform.position -= transform.up * Time.deltaTime * _gravity;

            _respawnTimer -= Time.deltaTime;

            if(_respawnTimer <= 0)
            {
                Respawn();
            }

            return;
        }

		if(_stepActive)
        {
            _fallTimer -= Time.deltaTime;

            if(_fallTimer <= 0)
            {
                _falling = true;
            }
        }
	}

    private void OnCollisionEnter(Collision hit)
    {
        if (_falling) return;

        if (hit.collider.gameObject.layer == LayerMask.NameToLayer("Player"))
        {
            if (fallMode == FallMode.Step)
            {
                _stepActive = true;
            }
        }
    }

    private void OnCollisionStay(Collision hit)
    {
        if (_falling) return;

        if (hit.collider.gameObject.layer == LayerMask.NameToLayer("Player"))
        {
            if (fallMode == FallMode.Contact)
            {
                _fallTimer -= Time.deltaTime;

                if (_fallTimer <= 0)
                {
                    _falling = true;
                }
            }
        }
    }

    private void OnCollisionExit(Collision hit)
    {
        if (_falling) return;
        
        if (hit.collider.gameObject.layer == LayerMask.NameToLayer("Player"))
        {
            if (fallMode == FallMode.Contact)
            {
                Reset();
            }
        }
    }

    public void Reset()
    {
        _falling = false;
        _stepActive = false;
        _fallTimer = fallTimer;
        _respawnTimer = respawnTimer;
    }

    public void Respawn()
    {
        transform.position = _originalPosition;
        Reset();
    }
}
