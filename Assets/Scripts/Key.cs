﻿using UnityEngine;
using System.Collections;


public class Key : MonoBehaviour
{
    //Position where the key hides itself after being obtained by the player.
    private Vector3 hidePos = new Vector3(30000, 30000, 30000);

	private void OnTriggerEnter(Collider other)
	{
		if(other.gameObject.layer == LayerMask.NameToLayer("Player"))
        {
            GetComponentInParent<Door>().OnObtainKey();
            transform.position = hidePos;
            gameObject.SetActive(false);
        }
	}
}
