﻿using UnityEngine;
using System.Collections;


public class FakePlatform : Platform
{
    /// <summary>
    /// It's a Lola.
    /// </summary>
    [Tooltip("It's a Lola!")]
    public Lola lola;

    private Collider[] colliders;

    protected override void Start()
	{
        base.Start();

        colliders = GetComponents<Collider>();
	}

    protected override void Update()
	{
        base.Update();

        for (int i = 0; i < colliders.Length; i++)
        {
            colliders[i].isTrigger = true;
            colliders[i].enabled = false;
        }
	}
}
