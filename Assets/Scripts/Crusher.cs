﻿using UnityEngine;
using System.Collections;


public class Crusher : Platform
{
    /// <summary>
    /// Speed at which the crusher crushes.
    /// Yo dawg, I herd you like crushers...
    /// </summary>
    [Tooltip("Speed at which the crusher crushes. \n Yo dawg, I herd you like crushers...")]
    public float crushSpeed = 1f;

    /// <summary>
    /// Does it kill the player when it touches it?
    /// </summary>
    [Tooltip("Does it kill the player when it touches it?")]
    public bool isLethal = false;

    [Tooltip("Does it stay active once the player touches the Trigger?")]
    public bool staysActive = false;

    //Piston movement range.
    private float _originalZ = -0.1f;
    private float _targetZ = -0.85f;
    private Transform _piston;

    //Indicates if the crusher is crushing.
    private bool _crushing = false;
    private bool _justCrushed;

    //Time for interpolation of movement.
    private float _time = 1f;
    private float _currentTime = 0;
    private float _perc = 0;
	
	protected override void Start()
	{
        base.Start();

        _piston = transform.FindChild("Piston");
	}

    protected override void Update()
	{
        base.Update();

        _time = (_targetZ - _originalZ) / crushSpeed;
        _time = Mathf.Abs(_time);

        if(!_justCrushed && _crushing)
        {
            _currentTime += Time.deltaTime;
            _perc = _currentTime / _time;

            _piston.localPosition = Vector3.Lerp(Vector3.forward * _originalZ, Vector3.forward * _targetZ, _perc);

            if(_perc >= 1)
            {
                if(!staysActive) _crushing = false;
                _justCrushed = true;
                return;
            }
        }
        else if(_justCrushed)
        {
            _currentTime -= Time.deltaTime;
            _perc = _currentTime / _time;

            _piston.localPosition = Vector3.Lerp(Vector3.forward * _originalZ, Vector3.forward * _targetZ, _perc);

            if (_perc <= 0)
            {
                _justCrushed = false;
                return;
            }
        }
        else
        {
            _perc = 0;
            _currentTime = 0;
        }
	}

    public void OnCrusherHeadTouch(PlayerTest pPlayer)
    {
        if (isLethal)
        {
            pPlayer.Respawn();
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (!_justCrushed && other.gameObject.layer == LayerMask.NameToLayer("Player"))
        {
            _crushing = true;
        }
    }
}
