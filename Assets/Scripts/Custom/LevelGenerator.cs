﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(SpectrumAnalyser))]
public class LevelGenerator : MonoBehaviour {

    [Header("Platform Settings")]
    public float timeToSpawnPlatform = 0.5f;
    public float platformDistance = 5f;

    [Header("Floor Settings")]
    public float timeToSpawnFloor = 0.5f;
    public float floorDistance = 5f;

    [Header("Prefabs")]
    public GameObject floor;
    public GameObject platform;
    public GameObject[] hazards;

    // Mierda
    private float _currentTimeToSpawnPlatform;
    private float _currentTimeToSpawnFloor;
    private SpectrumAnalyser _spectrum;
    private float _currentPlatformDistance;
    private float _currentFloorDistance;

    private bool endMap = false;


    void Awake()
    {
        _spectrum = GetComponent<SpectrumAnalyser>();

        _currentPlatformDistance = platformDistance;
        _currentFloorDistance    = floorDistance;

        _currentTimeToSpawnPlatform = timeToSpawnPlatform;
        _currentTimeToSpawnFloor    = timeToSpawnFloor;
    }


    void Update()
    {

        // PISO
        _currentTimeToSpawnFloor -= Time.deltaTime;

        if (_currentTimeToSpawnFloor < 0)
        {
            SpawnFloor();
            _currentTimeToSpawnFloor = timeToSpawnFloor;
        }

        if (endMap || _spectrum.timeRemaining <= 4f)
        {
            endMap = true;
            return;
        }

        // Plataformas
        _currentTimeToSpawnPlatform -= Time.deltaTime;

        if (_currentTimeToSpawnPlatform < 0)
        {
            SpawnPlatform();
            SpawnHazard();
            _currentTimeToSpawnPlatform = timeToSpawnPlatform;
        }
    }

    
    private void SpawnFloor()
    {
        var basicDistance = new Vector3(_currentFloorDistance, 0);

        Instantiate(floor, basicDistance, Quaternion.identity, transform);

        _currentFloorDistance += floorDistance;
    }


    private void SpawnPlatform()
    {
        var basicDistance = new Vector3(_currentPlatformDistance, Random.Range(1,7));
        
        // Platform
        var newPlatform = (GameObject)Instantiate(platform, basicDistance, Quaternion.identity, transform);

        newPlatform.transform.localScale += new Vector3(1f, 0, 0);

        // Sumo la nueva distancia
        _currentPlatformDistance += platformDistance;
    }

    private void SpawnHazard()
    {

        if (_spectrum.max > 80)
        {
            var hazardDistance = new Vector3(_currentPlatformDistance + platformDistance / 2, Random.Range(1, 7));
            var newHazard = (GameObject)Instantiate(hazards[Random.Range(0, 2)], hazardDistance, Quaternion.identity, transform);
            newHazard.transform.localScale += new Vector3(1f, 0, 0);
        }
        else if (_spectrum.max > 50)
        {
            var hazardDistance = new Vector3(_currentPlatformDistance + platformDistance / 2, Random.Range(1, 7));
            var newHazard = (GameObject)Instantiate(hazards[Random.Range(2, 5)], hazardDistance, Quaternion.identity, transform);
                newHazard.transform.localScale += new Vector3(1f, 0, 0);
        }
    }
}
