﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class DebugSong : MonoBehaviour {

    [Header("Adelantar Musica")]
    public KeyCode parte1 = KeyCode.Keypad1;
    public KeyCode parte2 = KeyCode.Keypad2;
    public KeyCode parte3 = KeyCode.Keypad3;

    [Header("Reiniciar Nivel")]
    public KeyCode reiniciar = KeyCode.R;

    [Header("Reiniciar Nivel")]
    public KeyCode menu = KeyCode.Escape;

    private SpectrumAnalyser _spectrum;

    void Awake()
    {
        _spectrum = GetComponent<SpectrumAnalyser>();
    }

    void Update()
    {
        if (Input.GetKeyDown(parte1))
        {
            _spectrum.songTime = _spectrum.songLength * 0.3f;
        }
        else if (Input.GetKeyDown(parte2))
        {
            _spectrum.songTime = _spectrum.songLength * 0.6f;
        }
        else if (Input.GetKeyDown(parte3))
        {
            _spectrum.songTime = _spectrum.songLength * 0.9f;
        }
        else if (Input.GetKeyDown(reiniciar))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
        else if (Input.GetKeyDown(menu))
        {
            SceneManager.LoadScene(0);
        }
    }
}
