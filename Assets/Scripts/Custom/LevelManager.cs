﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour {

    public void CargarPirate()
    {
        SceneManager.LoadScene((int)SceneID.PIRATE);
    }

    public void CargarShaolin()
    {
        SceneManager.LoadScene((int)SceneID.SHAOLIN);
    }

    public void CargarNoScared()
    {
        SceneManager.LoadScene((int)SceneID.NOSCARED);
    }
}

public enum SceneID
{
    MENU = 0,
    PIRATE,
    SHAOLIN,
    NOSCARED
}