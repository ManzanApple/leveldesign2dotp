﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(AudioSource))]
public class SpectrumAnalyser : MonoBehaviour
{

    /*public float timeToGetSamples = 0.5f;

    private float currentTimeToGetSamples;*/

    // Audio Spectrum Data
    private float[] _samples;
    private float _max = 0;
    private float _lastMax = 0;

    private AudioSource _audioSorce;
    
    void Awake()
    {
        _audioSorce = gameObject.GetComponent<AudioSource>();

        if (!_audioSorce) gameObject.AddComponent<AudioSource>();

        //currentTimeToGetSamples = timeToGetSamples;
    }

    void Update()
    {
        /*currentTimeToGetSamples -= Time.deltaTime;

        if (currentTimeToGetSamples < 0)
        {
            AnalyzeSpectrum();
            currentTimeToGetSamples = timeToGetSamples;
        }*/
    }

    void AnalyzeSpectrum()
    {
        _lastMax = _max;
        _max = 0;
        _samples = new float[1024];

        _audioSorce.GetOutputData(_samples, 0);


        foreach (float sample in _samples)
        {
            if (sample > _max) _max = sample;
        }
    }

    public float songLength
    {
        get
        {
            return _audioSorce.clip.length;
        }
    }

    public float songTime
    {
        get
        {
            return _audioSorce.time;
        }
        set
        {
            _audioSorce.time = value;
        }
    }

    public float timeRemaining
    {
        get
        {
            return songLength - songTime;
        }
    }

    public float max
    {
        get
        {
            AnalyzeSpectrum();
            return _max * 100;
        }
    }

    public float lastMax
    {
        get
        {
            AnalyzeSpectrum();
            return _lastMax * 100;
        }
    }


    public float[] samples
    {
        get
        {
            return _samples;
        }
    }
}