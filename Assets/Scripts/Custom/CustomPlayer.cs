﻿using UnityEngine;
using System.Collections;
using UnityEditor;


public class CustomPlayer : MonoBehaviour
{
    public float speed = 7f;
    public float jumpForce = 3f;
    public float gravityMultiplier = 1f;

    private Vector3 _velocity = new Vector3(0, 0, 0);
    private Rigidbody _rb;

    private bool _grounded = false;

    public bool deletePreviousCheckpoints = false;

    private void Awake()
    {
        Checkpoint.deletePreviousCheckpoints = deletePreviousCheckpoints;
        GenerateSpawnPointCheckpoint();
    }

    private void Start()
    {
        _rb = GetComponent<Rigidbody>();
    }

    private void GenerateSpawnPointCheckpoint()
    {
        GameObject c = (GameObject)Instantiate((GameObject)AssetDatabase.LoadAssetAtPath("Assets/Prefabs/Checkpoint.prefab", typeof(GameObject)),
                                                transform.position,
                                                Quaternion.identity);
        c.name = "Checkpoint (Spawn Point) - AUTO GENERATED";
        Checkpoint.SetActiveCheckpoint(c.GetComponent<Checkpoint>());
    }

    private void Respawn()
    {
        _rb.position = Checkpoint.activeCheckpoint.transform.position;
    }

    public void Die()
    {
        Respawn();
    }

    private void FixedUpdate()
    {
        ResetRigidbodiesVelocities();
        CalculateHorizontalVelocity();
        CalculateVerticalVelocity();
        CheckFloor();
        Move();
    }

    public void TransferVelocity(Vector3 velocity)
    {
        _velocity += velocity;
    }

    private void ResetRigidbodiesVelocities()
    {
        _rb.velocity = Vector3.zero;
    }

    private void CalculateHorizontalVelocity()
    {
        _velocity.x = speed * Time.deltaTime;
    }

    private void CalculateVerticalVelocity()
    {
        float j = Jump();
        _velocity.y += j > 0 ? j - (Physics.gravity.y / 15 * gravityMultiplier * Time.deltaTime) : (Physics.gravity.y / 15 * gravityMultiplier * Time.deltaTime);
    }

    private float Jump()
    {
        float v = 0;
        float jumpingMultiplier = Input.GetAxis("Jump");
        if (_grounded && jumpingMultiplier > 0)
        {
            _grounded = false;
            v = jumpForce * Time.deltaTime * jumpingMultiplier;
        }
        return v;
    }

    private Vector3 _groundPos = new Vector3(0, 0, 0);
    private float _distToGround = .51f;

    private void CheckFloor()
    {
        Debug.DrawRay(transform.position, -transform.up * _distToGround, Color.cyan);
        RaycastHit hit;
        if (Physics.Raycast(transform.position, -transform.up, out hit, _distToGround, 1 << LayerMask.NameToLayer("Terrain") | 1 << LayerMask.NameToLayer("Platform")))
        {
            if (_velocity.y > 0) return;
            _groundPos.x = _rb.position.x;
            _groundPos.y = hit.point.y + _distToGround - 0.01f;
            _groundPos.z = _rb.position.z;
            _rb.position = _groundPos;
            _grounded = true;
            _velocity.y = 0;
        }
        else
        {
            _grounded = false;
        }
    }

    private void Move()
    {
        _rb.position += _velocity;
    }
}
