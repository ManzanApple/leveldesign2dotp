﻿using UnityEngine;
using System.Collections;


public class DeathZone : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.layer == LayerMask.NameToLayer("Player"))
        {
            other.GetComponent<Player>().Die();
        }
    }

    private BoxCollider bc;
    private Vector3 _colliderGizmoSize;

    private void OnDrawGizmos()
    {
        bc = GetComponent<BoxCollider>();
        _colliderGizmoSize = bc.size;
        Gizmos.DrawIcon(transform.position, "death icon.png");
        Gizmos.color = Color.red;
        Gizmos.DrawWireCube(transform.position, _colliderGizmoSize);
    }
}
