﻿using UnityEngine;
using System.Collections;


public class RotatingPlatform : Platform
{
    /// <summary>
    /// Number of spins this platform will make before stopping again.
    /// </summary>
    [Tooltip("Number of spins this platform will make before stopping again.")]
    public int spins = 1;

    /// <summary>
    /// The amount of time it takes this platform to complete "spins".
    /// </summary>
    [Tooltip("The amount of time it takes this platform to complete 'Spins'.")]
    public float spinTime = 1f;

    /// <summary>
    /// Interval between the end of the last spin ans the beginning of the next one.
    /// </summary>
    [Tooltip("Interval between the end of the last spin and the beginning of the next one.")]
    public float spinInterval = 1f;
    private float _spinInterval;

    /// <summary>
    /// Axis in which this platform spins.
    /// </summary>
    public enum RotationAxis
    {
        X, Y, Z
    }
    [Tooltip("Axis in which this platform spins.")]
    public RotationAxis rotationAxis;

    //Vector that determines rotation speed and direction, among other factors.
    private Vector3 _rotationVector;
    private Vector3 _originalEulerAngles;

    //Sets this platform to spin or not.
    private bool _spin = false;

    //Angles the platform spun;
    private float _anglesSpun;

    protected override void Start()
	{
        base.Start();

        switch(rotationAxis)
        {
            case RotationAxis.X:
                _rotationVector = Vector3.right;
                break;

            case RotationAxis.Y:
                _rotationVector = Vector3.up;
                break;

            case RotationAxis.Z:
                _rotationVector = Vector3.forward;
                break;
        }

        _spinInterval = spinInterval;
        _originalEulerAngles = transform.eulerAngles;
	}

    protected override void Update()
	{
        base.Update();

        if(_spin)
        {
            float _axisVelocity = Time.deltaTime * (spins * 360 / spinTime);
            _anglesSpun += _axisVelocity;

            if(_anglesSpun < spins * 360)
            {
                Vector3 velocity = _rotationVector * _axisVelocity;
                transform.Rotate(velocity);
            }
            else
            {
                Reset();
            }
        }
        else
        {
            _spinInterval -= Time.deltaTime;

            if (_spinInterval <= 0)
            {
                _spin = true;
            }
        }
	}

    public void Reset()
    {
        _spin = false;
        _spinInterval = spinInterval;
        _anglesSpun = 0;
        transform.eulerAngles = _originalEulerAngles;
    }
}
