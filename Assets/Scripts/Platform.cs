﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody))]
public class Platform : MonoBehaviour
{
    protected Rigidbody _rb;

    protected virtual void Start()
    {
        _rb = GetComponent<Rigidbody>();
    }

	protected virtual void Update()
	{
       _rb.isKinematic = true;
	}
}
